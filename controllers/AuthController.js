const requestJson = require('request-json');
const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujcsb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var query = 'q={"email":"'+ req.body.email +'"}';
  console.log("La consulta es " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body) {
      var response = {};
      console.log(body);
       if(body.length > 0) {
         var usuario = body[0];
         if(crypt.checkPassword(req.body.password, usuario.password)) {
           var putBody = '{"$set":{"logged":true}}';
           httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(err,resMLab,body) {
             console.log(body);
             response.msg = "Login correcto";
             response.id = usuario.id;
             res.send(response);
           })
         } else {
           response.msg = "Login incorrecto";
           res.status(401);
           res.send(response);
         }
       } else {
         response.msg = "Login incorrecto";
         res.status(401);
         res.send(response);
       }

    }
  )
}


function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");
  console.log("Logout:" + req.params.id);

  var query = 'q={"id":'+ req.params.id +'}';
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body) {
      var response = {};
      if(body.length > 0) {
        if(body[0].logged) {
          var putBody = '{"$unset":{"logged":""}}'
          httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(err,resMLab,body) {
            console.log(body);
            response.msg = "Logout correcto";
            res.send(response);
          })
        } else {
          response.msg = "Logout incorrecto";
          res.send(response);
        }

      } else {
        response.msg = "Logout incorrecto";
        res.send(response);
      }
    })
}


module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
