const requestJson = require('request-json');
const crypt = require('../crypt');
// const io = require('../io');


 const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujcsb/collections/";
 const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//
// function getUsersV1(req, res) {
//   console.log("GET /apitechu/v1/users");
//   // res.sendFile('usuarios.json',{root: __dirname});
//
//   var users = require('../usuarios.json');
//
//   var response = {};
//
//   if(req.query.$count == "true") {
//       console.log(req.query.$count)
//       var len = users.length;
//       console.log(len);
//       response.count = len;
//   }
//   if(req.query.$top) {
//       console.log(req.query.$top)
//       var subconjunto = users.slice(0, req.query.$top);
//       response.users = subconjunto;
//   } else {
//       response.users = users;
//   }
//
//   res.send(response);
// }
//
//
// function getUsersV2(req, res) {
//   console.log("GET /apitechu/v2/users");
//
//   var httpClient = requestJson.createClient(baseMLabURL);
//   console.log("Cliente creado.");
//   httpClient.get(
//     "user?" + mLabAPIKey,
//     function(err,resMLab,body) {
//       var response = !err ? body : {
//         "msg": "Error obteniendo usuarios"
//       }
//       res.send(response);
//     }
//   )
// }
//
function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":'+ id +'}';
  console.log("La consulta es " + query);
  var httpClient = requestJson.createClient(baseMLabURL);

   httpClient.get(
     "usuarios?" + query + "&" + mLabAPIKey,
     function(err,resMLab,body) {
       if(err) {
         var response = {"msg": "Error obteniendo usuario"};
         res.status(500);
       } else {
        if(body.length > 0) {
          var response = body[0];
        } else {
          var response = {"msg": "Error obteniendo usuario"};
          res.status(404);
        }
       }
       res.send(response);
     }
   )
}

function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");
  console.log("id es " + req.body.id);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  console.log("phone es " + req.body.phone);
  console.log("address es " + req.body.address);
  console.log("password es " + req.body.password);



  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("usuarios?c=true&" + mLabAPIKey, function(err,resMLab,body) {
    if(err) {
      res.status(500).send({"msg": "Error creando usuario"});
    } else {
      console.log(body);
      var id = body + 1;
      console.log(id);
      var newUser = {
        "id": id,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "phone": req.body.phone,
        "address": req.body.address,
        "password": crypt.hash(req.body.password)
      }
      console.log(newUser);
      httpClient.post(
         "usuarios?" + mLabAPIKey, newUser,
         function(err,resMLab,body) {
           console.log("Usuario guardado");
           res.status(201).send({"msg": "Usuario creado.", "id":id});
         }
       )
    }
  }
  )


}

// function deleteUserV2(req, res) {
//   console.log("DELETE /apitechu/v2/users/:id");
//   console.log("El id del usuario a borrar es " + req.params.id);
//
//   var id = req.params.id;
//   var query = 'q={"id":'+ id +'}';
//   var newUser = [];
//   var httpClient = requestJson.createClient(baseMLabURL);
//
//   httpClient.put("user?" +query + "&" + mLabAPIKey, newUser,
//   function(err,resMLab,body) {
//     if(body.removed > 0) {
//       console.log("Usuario Eliminado");
//       res.status(200).send({"msg": "Usuario eliminado."});
//     } else {
//       console.log("Error borrando usuario");
//       res.status(404).send({"msg": "Error borrando usuario."});
//     }
//
//   })
// }
//
// function createUserV1(req, res) {
//   console.log("POST /apitechu/v1/users");
//   console.log(req.body.first_name);
//   console.log(req.body.last_name);
//   console.log(req.body.email);
//
//   var newUser = {
//     "id": req.body.id,
//     "first_name": req.body.first_name,
//     "last_name": req.body.last_name,
//     "email": req.body.email,
//   }
//   var users = require('../usuarios.json');
//   users.push(newUser);
//   console.log("Usuario añadido al array.")
//
//   io.writeUserDataToFile(users);
//   console.log("Proceso creación usuario terminado.")
//
//   res.send({"msg": "Usuario creado."});
// }
//
// function deleteUserV1(req, res) {
//   console.log("DELETE /apitechu/v1/users/:id");
//   console.log("El id del usuario a borrar es " + req.params.id);
//
//   var users = require('../usuarios.json');
//   // users.splice(req.params.id - 1, 1);
//
//   //ALTERNATIVA 1
//   // for(i=0;i<users.length;i++) {
//   //   var user = users[i];
//   //   if(user.id == req.params.id) {
//   //     console.log("Eliminamos el usuario.")
//   //     console.log(user);
//   //     users.splice(i,1);
//   //   }
//   // }
//
//   //ALTERNATIVA 2
//   // users.forEach(function(user) {
//   //   if(user.id == req.params.id) {
//   //     console.log("Eliminamos el usuario.")
//   //     console.log(user);
//   //     users.splice(users.indexOf(user),1);
//   //   }
//   // });
//
//   //ALTERNATIVA 3
//     // for (const prop in users) {
//     //   if(users[prop].id == req.params.id) {
//     //     console.log("Eliminamos el usuario.")
//     //     console.log(users[prop]);
//     //     users.splice(prop,1);
//     //   }
//     // }
//   //ALTERNATIVA 4
//   // for(const user of users) {
//   //   if(user.id == req.params.id) {
//   //     console.log("Eliminamos el usuario.")
//   //     console.log(user);
//   //     users.splice(users.indexOf(user),1);
//   //   }
//   // }
//   //ALTERNATIVA 5
//    var index = users.findIndex(function(element) {
//       return (element.id == req.params.id);
//     }
//   )
//   users.splice(index,1);
//
//   console.log("Usuario borrado.")
//   io.writeUserDataToFile(users);
//   console.log("Proceso borrado usuario terminado.")
//
//   res.send({"msg": "Usuario borrado."});
// }
//
// module.exports.getUsersV1 = getUsersV1;
// module.exports.getUsersV2 = getUsersV2;
// module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
// module.exports.deleteUserV1 = deleteUserV1;
 module.exports.getUserByIdV2 = getUserByIdV2;
// module.exports.deleteUserV2 = deleteUserV2;
