const requestJson = require('request-json');
//const io = require('../io');
//const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujcsb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getAccountsByUserIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts");

  var id = req.params.id;
  var query = 'q={"userId":'+ id +'}';
  var httpClient = requestJson.createClient(baseMLabURL);

   httpClient.get(
     "cuentas?" + query + "&" + mLabAPIKey,
     function(err,resMLab,body) {
       if(err) {
         var response = {"msg": "Error obteniendo cuentas"};
         res.status(500);
       }
       res.send(body);
     }
   )
}

function createAccountV2(req, res) {
  console.log("POST /apitechu/v2/accounts");
  console.log("userId es " + req.body.userId);
  console.log("iban es " + req.body.iban);
  console.log("desc es " + req.body.desc);
  console.log("balance es " + req.body.balance);
  var account = {
    "userId": req.body.userId,
    "iban": req.body.iban,
    "desc": req.body.desc,
    "balance": req.body.balance
  }
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post(
     "cuentas?" + mLabAPIKey, account,
     function(err,resMLab,body) {
       console.log("Cuenta creada");
       res.status(201).send({"msg": "Cuenta creada."});
     }
   )
}

function getMovementsByAccountIdV2(req, res) {
  console.log("GET /apitechu/v2/accounts/:iban/movements");

  var accountId = req.params.iban;
  var query = 'q={"iban":"'+ accountId +'"}';
  console.log(query);
  var httpClient = requestJson.createClient(baseMLabURL);

   httpClient.get(
     "movimientos?" + query + "&s={'timestamp':-1}&" + mLabAPIKey,
     function(err,resMLab,body) {
       if(err) {
         var response = {"msg": "Error obteniendo cuentas"};
         res.status(500);
       }
       console.log(body);
       res.send(body);
     }
   )
}

function createMovementV2(req, res) {
  console.log("POST /apitechu/v2/movements");
  console.log("iban es " + req.body.iban);
  console.log("timestamp es " + req.body.timestamp);
  console.log("desc es " + req.body.desc);
  console.log("amount es " + req.body.amount);
  console.log("categoryCode es " + req.body.categoryCode);
  console.log("categoryDesc es " + req.body.categoryDesc);

  var newMovement = {
    "iban": req.body.iban,
    "timestamp": req.body.timestamp,
    "desc":req.body.desc,
    "amount" : {"value":req.body.amount, "currency":"EUR"},
    "categoryCode": req.body.categoryCode,
    "categoryDesc": req.body.categoryDesc
  }
  var balance = 0;
   var query = 'q={"iban":"'+ req.body.iban +'"}';
   var httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get(
     "movimientos?" + query + "&" + mLabAPIKey,
     function(err,resMLab,body) {
       if(err) {
         res.status(500).send({"msg": "Error al crear movimiento."});
       } else {
         for(const movement of body) {
           balance = balance + movement.amount.value;
          }
          console.log("Balance:" + balance);
          balance = balance + newMovement.amount.value;
          newMovement.accountAmount = {"value":balance, "currency":"EUR"};

          httpClient.post(
             "movimientos?" + mLabAPIKey, newMovement,
             function(err,resMLab,body) {
               if(err) {
                 res.status(500).send({"msg": "Error al crear movimiento."});
               }
               console.log("Movimiento creado");

               var query = 'q={"iban":"'+ req.body.iban +'"}';
               var putBody = '{"$set":{"balance":'+balance+'}}';
               httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(err,resMLab,body) {
                 if(err) {
                   res.status(500).send({"msg": "Error al crear movimiento."});
                 }
                 console.log("Balance actualizado:" + balance);
                 res.status(201).send({"msg": "Movimiento creado."});
               })
             }
           )
       }
     }
   )



}

module.exports.getAccountsByUserIdV2 = getAccountsByUserIdV2;
module.exports.createAccountV2 = createAccountV2;
module.exports.getMovementsByAccountIdV2 = getMovementsByAccountIdV2;
module.exports.createMovementV2 = createMovementV2;
